'use strict';
const Librus = require("librus-api");
const {login, pass} = require('./config');

let client = new Librus();
// client.authorize("3801253", "Bartek06").then(function () {
client.authorize(login, pass).then(function () {
  // // List all subjects
  // client.homework.listSubjects().then(data => { console.log(data)});
  //
  // // List announcements
  // client.inbox.listAnnouncements().then(data => {console.log(data)});

  // List all e-mails in folder(5) in page(2)
  client.inbox.listInbox(5).then(data => {
  });

  client.inbox.getMessage(5, 652207).then(messageData => {
    console.log(messageData);
    for (let f of messageData.files) {
      client.inbox.getFile(f.path).then(response => response.pipe(fs.createWriteStream(f.name)))
    }
  });

});