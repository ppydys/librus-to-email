Simple wrapper based on [https://github.com/Mati365/librus-api](https://github.com/Mati365/librus-api) 
package which forwards messages from librus to email address


Usage
-----------------

1. clone the code
2. install dependencies: `npm install` 
3. provide credentials using `.env.dist` as a template. Create `.env`  file based on `.env.dist`
4. run the app `node app.js`



